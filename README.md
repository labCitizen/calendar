# Calendar

## Notes

sawayo.png: Visualizes the algorithm idea \
controller_communication: List of controller methods

```bash
notes/sawayo.png
notes/methods_overview/controller_communication.pdf
```

<br>

## Install dependencies

```bash
npm install
```

<br>

## Start webserver

Run command to start webserver on port 9000

```bash
npm run webpack
```

<br>

## Compile code

Compile code using webpack bundler (not necessary)

```bash
npm run build
```

<br>

## Create documentation

Creates a documentation based on JSDoc

```bash
npm run doc
```

<br>
<br>

# Terminology

## calendar

Original data containing events
<br><br>

## calendar_sorted (type Calendar)

Sorted calendar
<br><br>

## calendar_converted (type Events_Converted)

Converted sorted calendar for the DOM
<br><br>

## calendar_DOM (type Events_Converted)

Superset of converted calendar extending left and overlap values with real values

//@ts-ignore
import { dataController } from './modules/data.controller.ts'
//@ts-ignore
import { UIcontroller } from './modules/UI.controller.ts'

const controller = (function (dataCtrl, UIctrl) {
  /** Contains all events for the day
   * @key from: Time the event starts
   * @key to: Time the event ends
   * @key title: Name of the event
   */
  let calendar = [
    { from: 9, to: 14, title: 'Break' },
    { from: 10, to: 16, title: 'Work' },
    { from: 17, to: 20, title: 'Meeting' },
    { from: 17, to: 18, title: 'Networking' },
    { from: 18, to: 19, title: 'Presentation' },
    { from: 19, to: 21, title: 'Coding' }
  ]

  const clear_DOM = function () {
    UIctrl.clear_DOM()
  }

  const setup_listener = function () {
    const DOM = UIctrl.DOM_strings()
    const events = document.querySelectorAll(DOM.event)
    events.forEach((event) => event.addEventListener('click', delete_event))
  }

  const delete_event = function (event: any) {
    const event_title = event.target.textContent
    calendar = calendar.filter((event) => !event_title.includes(event.title))
    controller.init()
  }

  const display_calendar = function () {
    const calendar_original = calendar
    const calendar_sorted = dataCtrl.sort_calendar(calendar_original)
    const events = calendar_sorted.length

    // GUARD
    if (!events) {
      UIctrl.show_message('No meetings today :-)')
    }

    const calendar_converted = dataCtrl.convert_to_pixel(calendar_sorted)
    const calendar_DOM = dataCtrl.calculate_overlaps(calendar_converted)
    UIctrl.insert_events(calendar_DOM)
  }

  return {
    init: function () {
      clear_DOM()
      display_calendar()
      setup_listener()
    }
  }
})(dataController, UIcontroller)

controller.init()

/**
 * Orginal data
 */
export type Calendar = {
  from: number
  to: number
  title: string
}

/**
 * Converted original data into pixel values for the DOM
 */
export type Events_Converted = {
  top: number
  left: number
  height: number
  overlap: number
  time: string
  title: string
}

/**
 * A cell of the overlaps array that represents an event
 */
export type Overlap = string | null

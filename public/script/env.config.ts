/**
 * @key INDENTION: Number of pixels an event is indented
 * @key WORK_DAY: Size of the calendar in pixels
 * @key WORK_HOURS: Span of hours an event can be placed
 * @key WORK_DAY_START: Time the first event can be placed
 * @key WORK_HOUR: Returns 1 hour in pixels
 */
export const environment = {
  INDENTION: 15,
  WORK_DAY: 720,
  WORK_HOURS: 12,
  WORK_DAY_START: 9,
  WORK_HOUR: function () {
    return this.WORK_DAY / this.WORK_HOURS
  }
}

//@ts-ignore
import { Events_Converted } from '../data.types.ts'

export const UIcontroller = (function () {
  const DOMstrings = {
    calendar: 'calendar',
    event: '.js-event'
  }

  return {
    /**
     * Collection of DOM strings
     * @returns An object of DOM strings
     */
    DOM_strings: function () {
      return DOMstrings
    },

    /**
     * Used to display a message if no events take place
     * @param message String to be displayed
     */
    show_message: function (message: string) {
      const calendar = document.getElementById(DOMstrings.calendar)!
      calendar.textContent = message
    },

    /**
     * Inserts events from calendar
     * @param calendar_DOM Superset of Events_Converted containing proper indention and overlap values
     */
    insert_events: function (calendar_DOM: Events_Converted[]) {
      const events = calendar_DOM.length
      let div: HTMLDivElement
      let new_line: HTMLElement
      let calender_div: HTMLElement
      let title: Text
      let time: Text

      for (let i = 0; i < events; i++) {
        div = document.createElement('div')
        new_line = document.createElement('br')
        title = document.createTextNode(calendar_DOM[i].title)
        time = document.createTextNode(`(${calendar_DOM[i].time})`)

        div.appendChild(title)
        div.appendChild(new_line)
        div.appendChild(time)
        div.classList.add('js-event')

        div.style.cssText = `
        position: absolute;
        top: ${calendar_DOM[i].top / 10}rem;
        left: ${calendar_DOM[i].left / 10}rem;
        height: ${calendar_DOM[i].height / 10}rem;
        width: 100%;
        padding: 1rem 0 0 3rem;
        background-image: linear-gradient(to bottom, red 0%, red ${
          calendar_DOM[i].overlap / 10
        }rem, green ${calendar_DOM[i].overlap / 10}rem, green 100%);
        z-index: ${i};
        cursor: pointer;
        `

        calender_div = document.getElementById('calendar')!
        calender_div.appendChild(div)
      }
    },

    /**
     * Removes all event from the calendar
     */
    clear_DOM: function () {
      document.getElementById(DOMstrings.calendar)!.innerHTML = ''
    }
  }
})()

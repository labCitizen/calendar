//@ts-ignore
import { environment } from '../env.config.ts'
//@ts-ignore
import { Calendar, Events_Converted, Overlap } from '../data.types.ts'

export const dataController = (function () {
  const helper_methods = {
    /** Converts starting time into starting position
     * @used_by convert_to_pixel
     * @param event_start Time the event starts
     * @returns Position of starting time in pixel
     */
    convert_start_time: function (event_start: number): number {
      const tare_time = event_start - environment.WORK_DAY_START
      const event_start_px = tare_time * environment.WORK_HOUR()
      return event_start_px
    },

    /**
     * Converts duration into height
     * @used_by convert_to_pixel
     * @param event_duration Time the event takes place
     * @returns Height of duration in pixel
     */
    convert_duration: function (event_duration: number): number {
      const event_duration_px = event_duration * environment.WORK_HOUR()
      return event_duration_px
    },

    /**
     * Creates an array which visualizes when the events takes place
     * @used_by calculate_overlaps
     * @param calendar Contains values of the calendar that were already converted to pixel
     * @returns An array that represents the start, duration and end of an event
     */
    create_overlaps_array: function (calendar: Events_Converted[]): Overlap[] {
      const overlaps = new Array(13).fill(null)
      const events = calendar.length
      let start_time: number
      let tare_start_time: number
      let end_time: number
      let tare_end_time: number

      for (let i = 0; i < events; i++) {
        start_time = +calendar[i].time.split('-')[0]
        tare_start_time = start_time - environment.WORK_DAY_START
        end_time = +calendar[i].time.split('-')[1]
        tare_end_time = end_time - environment.WORK_DAY_START

        for (let j = tare_start_time; j <= tare_end_time; j++) {
          if (!overlaps[j]) {
            overlaps[j] = `${i}`
            continue
          }

          overlaps[j] += `${i}`
        }
      }

      return overlaps
    },

    /**
     * Calculates how much an event should be indented
     * @used_by calculate_overlaps
     * @param overlaps Representation of the events as array
     * @param key Current event that is looked for
     * @returns The indention of an event
     */
    calculate_indention: function (overlaps: Overlap[], key: number): number {
      let key_position: number | null
      let indention: number | null
      let indention_reset: number | null
      const overlaps_len = overlaps.length

      for (let i = 0; i < overlaps_len; i++) {
        key_position = overlaps[i]?.indexOf(key.toString()) || null
        indention_reset = overlaps[i + 1]?.indexOf(key.toString()) || null

        if (
          key_position &&
          key_position > 0 &&
          indention_reset &&
          indention_reset > 0
        ) {
          indention = key_position * environment.INDENTION
          indention *= indention / 2.5
          return indention
        }
      }

      return 0
    },

    /**
     * Calculates hoch much an event overlaps with an other event
     * @used_by calculate_overlaps
     * @param overlaps Representation of the events as array
     * @param key Current event that is looked for
     * @returns The height an event overlaps with another event
     */
    calculate_overlap: function (overlaps: Overlap[], key: number): number {
      // GUARD
      if (key === 0) {
        return 0
      }

      let first_occurence = -1
      let first_overlay = -1
      let last_overlay = -1
      let overlay_duration: number

      for (let i = 0; i < overlaps.length; i++) {
        // GET FIRST OCCURENCE
        if (first_occurence === -1 && overlaps[i]?.includes(key.toString())) {
          first_occurence = i
        }

        // GET FIRST OVERLAY
        if (
          first_overlay === -1 &&
          overlaps[i] &&
          overlaps[i]?.includes(key.toString()) &&
          // @ts-ignore: Object is possibly 'null'.
          overlaps[i]?.length > 1 &&
          overlaps[i]?.indexOf(key.toString()) != 0
        ) {
          first_overlay = i
        }

        // GET LAST OVERLAY
        if (
          overlaps[i] &&
          overlaps[i]?.includes(key.toString()) &&
          // @ts-ignore: Object is possibly 'null'.
          overlaps[i]?.length > 1 &&
          overlaps[i]?.indexOf(key.toString()) != 0
        ) {
          last_overlay = i
        }
      }

      // RETURN
      if (first_overlay != -1) {
        overlay_duration =
          (last_overlay - first_overlay) * environment.WORK_HOUR()
        return overlay_duration
      }

      return 0
    }
  }

  return {
    /**
     * Sorts all objects of the calendar by time
     * @param calendar Contains an array of events
     * @returns A sorted calendar
     */
    sort_calendar: function (calendar: Calendar[]): Calendar[] {
      return calendar.sort((a: Calendar, b: Calendar) => a.from - b.from)
    },

    /**
     * Converts values of calendar into proper pixel representations
     * @param calendar Contains an array of events
     * @returns An array of converted event objects
     */
    convert_to_pixel: function (calendar: Calendar[]): Events_Converted[] {
      const events_converted: Events_Converted[] = []
      let event_duration: number
      let event_start_pixel: number
      let event_duration_pixel: number

      for (let event of calendar) {
        event_duration = event.to - event.from
        event_start_pixel = helper_methods.convert_start_time(event.from)
        event_duration_pixel = helper_methods.convert_duration(event_duration)

        events_converted.push({
          top: event_start_pixel,
          left: 0,
          height: event_duration_pixel,
          overlap: 0,
          time: `${event.from}-${event.to}`,
          title: `${event.title}`
        })
      }

      return events_converted
    },

    calculate_overlaps: function (
      calendar: Events_Converted[]
    ): Events_Converted[] {
      const overlaps = helper_methods.create_overlaps_array(calendar)
      const events = calendar.length

      for (let i = 0; i < events; i++) {
        calendar[i].left = helper_methods.calculate_indention(overlaps, i) || 0
        calendar[i].overlap = helper_methods.calculate_overlap(overlaps, i) || 0
      }

      return calendar
    }
  }
})()
